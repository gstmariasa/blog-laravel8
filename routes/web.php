<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\AdmBlogController;
use App\Http\Controllers\AdmHomeController;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::prefix('blog')->group(function() {
    Route::get('/', [BlogController::class, 'index']);
    
    Route::get('detail/{id}',[BlogController::class, 'detail'])->name('blog.detail');
    
    Route::get('categori', [BlogController::class, 'kategori']);
});

Route::middleware('guest')->group(function() {
    Route::get('login', [AuthController::class, 'login'])->name('login');
    Route::post('login', [AuthController::class, 'doLogin'])->name('login.action');
});

Route::middleware('auth')->group(function() {
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('current-user-login', [AuthController::class, 'curentUser'])->name('login.currentUser');

    Route::middleware('role:admin')->prefix('admin')->group(function() {
        Route::get('/home', [AdmHomeController::class, 'index'])->name('admin.home.index');
        
        Route::prefix('blog')->group(function() {
            Route::get('/',[AdmBlogController::class, 'index'])->name('admin.blog.index');
            Route::get('create',[AdmBlogController::class, 'create'])->name('admin.blog.create');
            Route::post('save',[AdmBlogController::class, 'store'])->name('admin.blog.store');
            Route::get('edit/{id}',[AdmBlogController::class, 'edit'])->name('admin.blog.edit');
            Route::post('update/{id}',[AdmBlogController::class, 'update'])->name('admin.blog.update');
            Route::post('delete/{id}',[AdmBlogController::class, 'delete'])->name('admin.blog.delete');
        });
    });


    Route::middleware('role:user')->prefix('user')->group(function() {
        Route::get('home', function() {
            return "Untuk halaman user dengan role: user";
        });
    });

    Route::middleware('role:user|admin')->group(function() {
        Route::get('alluser-home', function() {
            return "Untuk halaman user dengan role: user dan admin";
        });
    });
});

