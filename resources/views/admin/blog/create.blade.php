@extends('layout.admin.master')

@section('page-header')
    Create Blog
@endsection

@section('custom-css')
    <style>
        .form-group {
            margin-bottom: 15px !important;
        }
    </style>
@endsection

@section('konten')
    <main>  
        <div class="container-fluid px-4">
            <h1 class="mt-4">Blog</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">Blog</li>
                <li class="breadcrumb-item active">Add Blog</li>
            </ol>
            <div class="card mb-4">
                <div class="card-header">
                    Tambah Blog
                </div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                   <form action="{{ route('admin.blog.store') }}" method="post">
                       @csrf
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" rows="10" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="category">Category</label>
                            <select name="category" id="" class="form-control">
                                <option value="IT">IT</option>
                                <option value="Life">Life</option>
                                <option value="Education">Education</option>
                                <option value="Healt">Healt</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                   </form>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('custom-js')
    
@endsection
