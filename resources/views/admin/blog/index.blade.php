@extends('layout.admin.master')

@section('page-header')
    Admin Blog
@endsection

@section('custom-css')

@endsection

@section('konten')
    <main>  
        <div class="container-fluid px-4">
            <h1 class="mt-4">Blog</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Blog</li>
            </ol>
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    DataTable Example
                </div>
                <div class="card-body">
                    <a href="{{ route('admin.blog.create') }}" class="btn btn-info">Add Blog</a>
                    <hr>
                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Categori</th>
                                <th>Author</th>
                                <th class="col-md-1">Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Categori</th>
                                <th>Author</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($blogs as $blog)
                            <tr>
                                <td>{{ $blog->title }}</td>
                                <td>{{ $blog->description }}</td>
                                <td>{{ $blog->category }}</td>
                                <td>{{ $blog->user->name }}</td>
                                <td>
                                    <a href="{{ route('admin.blog.edit', $blog->id) }}" class="btn btn-sm btn-primary" style="display: inline;">Edit</a>
                                    <form action="{{ route('admin.blog.delete', $blog->id) }}" method="post">
                                        @csrf
                                        <button class="btn btn-sm btn-danger" style="display: inline;">Delete</button>
                                    </form>                                    
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('custom-js')
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/js/datatables-simple-demo.js') }}"></script>
@endsection
