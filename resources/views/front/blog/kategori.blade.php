@extends('layout.front.master')


@section('page-header')
    Kategori Blog Ganeshcom
@endsection

@section('custom-css')

@endsection


@section('konten')
    <!-- Page Header-->
    <header class="masthead" style="background-image: url('{{ asset('front/assets/img/home-bg.jpg') }}')">
        <div class="container position-relative px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="site-heading">
                        <h1>Page Kategori</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <h1>Ini konten dari page kategori</h1>
@endsection


@section('custom-js')

@endsection

