<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;

class AdmBlogController extends Controller
{
    public function index() {
        $blogs = Blog::where('author', \Auth::user()->id)->get();
        return view('admin.blog.index', compact('blogs'));
    }

    public function create() {
        return view('admin.blog.create');
    }

    public function store(Request $request) {
        $validated = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'category' => 'required',
        ]);

        $data = [];
        $data['title'] = $request->title;
        $data['description'] = $request->description;
        $data['category'] = $request->category;
        $data['author'] = \Auth::user()->id;

        Blog::create($data);
        //bisa tambah notif
        return redirect()->route('admin.blog.index');
    }

    public function edit($id) {
        $blog = Blog::where('id', $id)->firstOrFail();
        return view('admin.blog.edit', compact('blog'));
    }

    public function update(Request $request, $id) {
        $blog = Blog::where('id', $id)->firstOrFail();
        $validated = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'category' => 'required',
        ]);

        $data = [];
        $data['title'] = $request->title;
        $data['description'] = $request->description;
        $data['category'] = $request->category;

        $blog->update($data);
        //bisa tambah notif
        return redirect()->route('admin.blog.index');
    }

    public function delete($id) {
        $blog = Blog::where('id', $id)->firstOrFail();
        $blog->delete();
        //bisa tambah notif
        return redirect()->route('admin.blog.index');
    }
}
