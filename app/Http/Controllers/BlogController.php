<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;

class BlogController extends Controller
{
    public function index() {
        $allBlog = Blog::get();
        return view('front.blog.index', compact('allBlog'));
    }

    public function detail($id) {
        $blog = Blog::where('id', $id)->firstOrFail();
        return view('front.blog.detail', compact('blog'));
    }

    public function kategori() {
        return view('front.blog.kategori');
    }

}
